/***************************************************************************
 *   Copyright (C) 2007 by Schpilka Yannick   *
 *   schpilka@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 *   In addition, as a special exception, the copyright holders give       *
 *   permission to link the code of this program with any edition of       *
 *   the Qt library by Trolltech AS, Norway (or with modified versions     *
 *   of Qt that use the same license as Qt), and distribute linked         *
 *   combinations including the two.  You must obey the GNU General        *
 *   Public License in all respects for all of the code used other than    *
 *   Qt.  If you modify this file, you may extend this exception to        *
 *   your version of the file, but you are not obligated to do so.  If     *
 *   you do not wish to do so, delete this exception statement from        *
 *   your version.                                                         *
 ***************************************************************************/
#include "node.h"
#include "graph.h"
#include <qwidgetstack.h>
#include <qpoint.h>
#include <qcursor.h>
#include <qpopupmenu.h>
#include <qdragobject.h>
#include <qinputdialog.h>
#include <qfiledialog.h>
#include <qpainter.h>
#include <qtextedit.h>

Node::Node(Graph *parent,QString id):QWidget(parent->viewport(),id){
	/** new instance de la stack */
	stack = new QWidgetStack(this);
	stack->setFixedSize(100,120);
	stack->move(0,20);
	stack->show();

	/** config du widget courant */
	setFixedSize(100,140);
	setAcceptDrops(true);

	initMenu();
	setPaletteBackgroundColor(Qt::black);
}


Node::~Node(){
}

void Node::setWidget(QWidget *w){
	stack->addWidget(w);
	stack->raiseWidget(w);
	w->show();
}

/** Deplacer un noeud */
void Node::mousePressEvent(QMouseEvent *event){
	
	if (event->button() == Qt::RightButton)
		context_menu->exec(QCursor::pos());
	else{
		move_start = event->pos();
		moving = true;
		emit endEdge(QString(name()));
	}
}

void Node::mouseMoveEvent(QMouseEvent *event){
	if (moving){
		QPoint p = pos() + (event->pos() - move_start);
                move( p );
		emit nodeMoved();
	}
}

void Node::mouseReleaseEvent(QMouseEvent *event){
	if (event->button() == Qt::LeftButton)
		moving = false;
}

void Node::mouseDoubleClickEvent(QMouseEvent *event){
	
}

/** Un objet arrive dans le noeud */
void Node::dragEnterEvent (QDragEnterEvent *event){
        if ((QColorDrag::canDecode(event)) || (QTextDrag::canDecode(event))
                || (QUriDrag::canDecode(event)))
                event->accept();
}

/** Un objet est drop dans le noeud */
void Node::dropEvent ( QDropEvent *event ){
        QColor c;
	if ( QColorDrag::decode(event, c) ){
                setPaletteBackgroundColor(c);
		context_menu->setPaletteForegroundColor(c);
        }
}

/** On ecrit le nom du noeud */
void Node::paintEvent (QPaintEvent *event){
        QPainter painter(this);
        QColor color = paletteBackgroundColor();

        // on inverse la couleur de fond pour ecrire le titre
        int r = 255 - color.red();
        int g = 255 - color.green();
        int b = 255 - color.blue();

        painter.setPen(QColor(r,g,b));
        painter.save();
        QRect area(0,0,100,120);
        QString text (name());
        text.truncate(10);
        painter.drawText(area,Qt::AlignHCenter,text);
        painter.restore();
}


void Node::initMenu(){
	context_menu = new QPopupMenu;
	context_menu->setCaption(QString(name()) + " menu");

	context_menu->insertItem("Draw link",this,SLOT(createEdge()),1);
	context_menu->insertItem("Link to ...",this,SLOT(linkTo()),2);
	context_menu->insertItem("Hello World !",this,SLOT(helloWorld()),3);
}

/** Emission du signal */
void Node::createEdge(){
        emit createEdge(QString(name()));
}

/** Emission du signal */
void Node::linkTo(){
	bool ok;
        QString tgt = QInputDialog::getText(
            "KGraphInterface Edge Edition", "Enter node target:", QLineEdit::Normal,
             QString::null, &ok, this );

       emit createEdge(QString(name()));
       emit endEdge(tgt);
}


void Node::helloWorld(){
	QWidget* w = new QTextEdit(this);
	w->append("Hello World");	

	this->setWidget(w);
}

/** Change la couleur de la stack */
void Node::setStackColor(QColor c){
        stack->setPaletteBackgroundColor(c.dark(150));
}

QString Node::toXml(){
	 QString res;
        /** Noeud*/
        res.append("\n		<node id=\"" + QString(name()) + "\" x=\"" + QString::number(pos().x(),10) + "\" y=\"" + QString::number(pos().y(),10) + "\" background=\"" + colorToString() + "\">");

	/** ajouter le rendu du widget contenu */

        res.append("\n		</node>");
        return res;

}

QString Node::colorToString(){
	QColor c = paletteBackgroundColor();
	return intToHString(c.red()) + intToHString(c.green()) + intToHString(c.blue());
}
		
QString Node::intToHString(int i){
	if (i < 16)
                return "0" + QString::number(i,16);
        else
                return QString::number(i,16);
}

#include "node.moc"