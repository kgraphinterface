/***************************************************************************
 *   Copyright (C) 2007 by Schpilka Yannick   *
 *   schpilka@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 *   In addition, as a special exception, the copyright holders give       *
 *   permission to link the code of this program with any edition of       *
 *   the Qt library by Trolltech AS, Norway (or with modified versions     *
 *   of Qt that use the same license as Qt), and distribute linked         *
 *   combinations including the two.  You must obey the GNU General        *
 *   Public License in all respects for all of the code used other than    *
 *   Qt.  If you modify this file, you may extend this exception to        *
 *   your version of the file, but you are not obligated to do so.  If     *
 *   you do not wish to do so, delete this exception statement from        *
 *   your version.                                                         *
 ***************************************************************************/
#ifndef GRAPH_H
#define GRAPH_H

#include <qscrollview.h>

/**
        @author Schpilka Yannick <yschpilka@gmail.com>
*/

class Node;
class Edge;
class QPopupMenu;
class QStringList;
class QPoint;

class Graph : public QScrollView
{
        Q_OBJECT
        public:
                Graph(QWidget *parent,QString id);

                ~Graph();

                /**Ajouter un noeud dans le graphe*/
                void operator<<( Node *node );

		void add (Node *n,QPoint p);
                /**Lie deux noeuds*/
                void link(Node *source, Node *cible );
		void link(QString source, QString cible );
                /** Dessin du graphe*/
                void paintEvent(QPaintEvent *ev);

		QString toXml ();
        public slots:
                /** Creation d'un noeud a l'aide de boite de dialogue*/
                void createNode();

                /** Creation d'un lien a l'aide de boite de dialogue*/
                void createEdge();

                /** Commencer la creation d'un lien a partir d'un noeud source*/
                void beginEdge(QString);

                /** Terminer la creation d'un lien a partir d'un noeud cible*/
                void endEdge(QString target);

                /** Annuler la creation d'un lien*/
                void cancelEdge();

                /** Montre une vue d'ensemble du graphe */
                void showOverview();

                /** Ouvrir une boite de dialogue de choix de couleur*/
                void openColorDlg();

                /** Met a jour le tooltip */
                void adjustTooltip();
        signals:
                void graphCreated();
                void graphChanged();
                void graphLoaded();
                void nodeAdded();
                void edgeCreated();
                void colorChanged();

        protected:


                /** On drag un noeud ou une couleur*/
                void dragEnterEvent(QDragEnterEvent *event);
                void dropEvent(QDropEvent *event);

                /** Pop du menu contextuel*/
                void mouseReleaseEvent(QMouseEvent *event);

                /** Quand on bouge la souris dans le graphe */
                void contentsMouseMoveEvent ( QMouseEvent * event);



        private:
                /** Menu contextuel */
                QPopupMenu *context_menu;

                /** Liste des noeuds */
                QStringList nodes;

                /** Liste des liens */
                QPtrList<Edge> edges;

                /** Flag a true quand on est mode dessin*/
                bool drawing;

                QPoint start_drawing_edge;
                QString source_drawing_edge;

                /** Initialisation du menu contextuel */
                void initContextMenu();

};

#endif