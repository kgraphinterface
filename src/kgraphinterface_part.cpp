
#include "kgraphinterface_part.h"

#include <kinstance.h>
#include <kaction.h>
#include <kstdaction.h>
#include <kfiledialog.h>
#include <kglobal.h>
#include <klocale.h>

#include <qfile.h>
#include <qtextstream.h>
#include <qxml.h>
#include "graph.h"
#include "gihandler.h"

KGraphInterfacePart::KGraphInterfacePart( QWidget *parentWidget, const char *widgetName,
                                  QObject *parent, const char *name )
    : KParts::ReadWritePart(parent, name)
{
    // we need an instance
    setInstance( KGraphInterfacePartFactory::instance() );

    // this should be your custom internal widget
    m_widget = new Graph( parentWidget, "Graphe" );

    // notify the part that this is our internal widget
    setWidget(m_widget);

    // create our actions
    KStdAction::open(this, SLOT(fileOpen()), actionCollection());
    KStdAction::saveAs(this, SLOT(fileSaveAs()), actionCollection());
    KStdAction::save(this, SLOT(save()), actionCollection());

    // set our XML-UI resource file
    setXMLFile("kgraphinterface_part.rc");

    // we are read-write by default
    setReadWrite(true);

    // we are not modified since we haven't done anything yet
    setModified(false);
}

KGraphInterfacePart::~KGraphInterfacePart()
{
}

void KGraphInterfacePart::setReadWrite(bool rw)
{
    // notify your internal widget of the read-write state
   /* m_widget->setReadOnly(!rw);
    if (rw)
        connect(m_widget, SIGNAL(textChanged()),
                this,     SLOT(setModified()));
    else
    {
        disconnect(m_widget, SIGNAL(textChanged()),
                   this,     SLOT(setModified()));
    }
*/
    ReadWritePart::setReadWrite(rw);
}

void KGraphInterfacePart::setModified(bool modified)
{
    // get a handle on our Save action and make sure it is valid
    KAction *save = actionCollection()->action(KStdAction::stdName(KStdAction::Save));
    if (!save)
        return;

    // if so, we either enable or disable it based on the current
    // state
    if (modified)
        save->setEnabled(true);
    else
        save->setEnabled(false);

    // in any event, we want our parent to do it's thing
    ReadWritePart::setModified(modified);
}

bool KGraphInterfacePart::openFile()
{
    // m_file is always local so we can use QFile on it
    QFile file(m_file);
    if (file.open(IO_ReadOnly) == false)
        return false;

	//On parse le fichier recu
        GIHandler handler (m_widget);
        QXmlSimpleReader reader;
        reader.setContentHandler(&handler);
        reader.setErrorHandler(&handler);
        QXmlInputSource source(&file);
        reader.parse(source);

    file.close();

    // now that we have the entire file, display it
   // m_widget->setText(str);

    // just for fun, set the status bar
    emit setStatusBarText( m_url.prettyURL() );

    return true;
}

bool KGraphInterfacePart::saveFile()
{
    // if we aren't read-write, return immediately
    if (isReadWrite() == false)
        return false;

    // m_file is always local, so we use QFile
    QFile file(m_file);
    if (file.open(IO_WriteOnly) == false)
        return false;

    // use QTextStream to dump the text to the file
    QTextStream stream(&file);
    stream << m_widget->toXml();

    file.close();

    return true;
}

void KGraphInterfacePart::fileOpen()
{
    // this slot is called whenever the File->Open menu is selected,
    // the Open shortcut is pressed (usually CTRL+O) or the Open toolbar
    // button is clicked
    QString file_name = KFileDialog::getOpenFileName();

    if (file_name.isEmpty() == false)
        openURL(file_name);
}

void KGraphInterfacePart::fileSaveAs()
{
    // this slot is called whenever the File->Save As menu is selected,
    QString file_name = KFileDialog::getSaveFileName();
    if (file_name.isEmpty() == false)
        saveAs(file_name);
}


// It's usually safe to leave the factory code alone.. with the
// notable exception of the KAboutData data
#include <kaboutdata.h>
#include <klocale.h>

KInstance*  KGraphInterfacePartFactory::s_instance = 0L;
KAboutData* KGraphInterfacePartFactory::s_about = 0L;

KGraphInterfacePartFactory::KGraphInterfacePartFactory()
    : KParts::Factory()
{
}

KGraphInterfacePartFactory::~KGraphInterfacePartFactory()
{
    delete s_instance;
    delete s_about;

    s_instance = 0L;
}

KParts::Part* KGraphInterfacePartFactory::createPartObject( QWidget *parentWidget, const char *widgetName,
                                                        QObject *parent, const char *name,
                                                        const char *classname, const QStringList &args )
{
    // Create an instance of our Part
    KGraphInterfacePart* obj = new KGraphInterfacePart( parentWidget, widgetName, parent, name );

    // See if we are to be read-write or not
    if (QCString(classname) == "KParts::ReadOnlyPart")
        obj->setReadWrite(false);

    return obj;
}

KInstance* KGraphInterfacePartFactory::instance()
{
    if( !s_instance )
    {
        s_about = new KAboutData("kgraphinterfacepart", I18N_NOOP("KGraphInterfacePart"), "0.1");
        s_about->addAuthor("Schpilka Yannick", 0, "schpilka@gmail.com");
        s_instance = new KInstance(s_about);
    }
    return s_instance;
}

extern "C"
{
    void* init_libkgraphinterfacepart()
    {
	KGlobal::locale()->insertCatalogue("kgraphinterface");
        return new KGraphInterfacePartFactory;
    }
};

#include "kgraphinterface_part.moc"
