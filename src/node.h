/***************************************************************************
 *   Copyright (C) 2007 by Schpilka Yannick   *
 *   schpilka@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 *   In addition, as a special exception, the copyright holders give       *
 *   permission to link the code of this program with any edition of       *
 *   the Qt library by Trolltech AS, Norway (or with modified versions     *
 *   of Qt that use the same license as Qt), and distribute linked         *
 *   combinations including the two.  You must obey the GNU General        *
 *   Public License in all respects for all of the code used other than    *
 *   Qt.  If you modify this file, you may extend this exception to        *
 *   your version of the file, but you are not obligated to do so.  If     *
 *   you do not wish to do so, delete this exception statement from        *
 *   your version.                                                         *
 ***************************************************************************/
#ifndef NODE_H
#define NODE_H

#include <qwidget.h>

/**
	@author Schpilka Yannick <schpilka@gmail.com>
*/

class Graph;
class QWidgetStack;
class QPopupMenu;

class Node : public QWidget{
	Q_OBJECT
	public:
   		Node(Graph *parent,QString id);
    		~Node();

		/** definit le widget contenu */
		void 	setWidget (QWidget *widget);

		/** definit la couleur de fond */
		void 	setStackColor(QColor c);
		
		/** renvoie le rendu xml du noeud */
		QString toXml();

	public slots:
		/** commencement du dessin d'un lien */
		void createEdge();

		/** ouvre un pop-up pour lier le noeud a un autre */
		void linkTo();

		/** pour montrer comment ajouter un simple widget */
		void helloWorld();

	signals:
		/** capte par le graphe pour redessiner les liens */
                void 	nodeMoved();
                void 	createEdge(QString);
                void 	endEdge(QString);


	protected:
		/** deplacer un noeud */
		void 	mousePressEvent(QMouseEvent *event);
		void 	mouseReleaseEvent(QMouseEvent *event);
		void 	mouseMoveEvent(QMouseEvent *event);
		void	mouseDoubleClickEvent(QMouseEvent *event);

		/** drag n drop d'infos */
                void 	dragEnterEvent (QDragEnterEvent *event);
                void 	dropEvent ( QDropEvent *event );

		/** raffraichissement du noeud */
                void 	paintEvent (QPaintEvent *event);

	private:
		/** pile qui contient l'unique widget */
		QWidgetStack *stack;

		/** fonctions utiles a mettre dans une autre classe */
		QString colorToString();
		QString intToHString(int i);

		/** menu contextuel */
                QPopupMenu *context_menu;
		void initMenu();

		/** flag a true quand le noeud est en deplacement */
		bool moving;

		/** point de depart d'un mouvement, pour le calcul de la nouvelle pos */
		QPoint move_start;
};

#endif