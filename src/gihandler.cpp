/***************************************************************************
 *   Copyright (C) 2007 by Schpilka Yannick   *
 *   schpilka@localhost   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 *   In addition, as a special exception, the copyright holders give       *
 *   permission to link the code of this program with any edition of       *
 *   the Qt library by Trolltech AS, Norway (or with modified versions     *
 *   of Qt that use the same license as Qt), and distribute linked         *
 *   combinations including the two.  You must obey the GNU General        *
 *   Public License in all respects for all of the code used other than    *
 *   Qt.  If you modify this file, you may extend this exception to        *
 *   your version of the file, but you are not obligated to do so.  If     *
 *   you do not wish to do so, delete this exception statement from        *
 *   your version.                                                         *
 ***************************************************************************/
#include "gihandler.h"
#include "graph.h"
#include "node.h"
#include <qmessagebox.h>

#include <qpushbutton.h>
#include <qtextedit.h>
#include <qimage.h>
#include <qpixmap.h>

GIHandler::GIHandler(Graph *g){
        graph = g;
	nbr = 0;
}


GIHandler::~GIHandler(){
}


bool GIHandler::startElement(const QString &,
                             const QString &,
                             const QString &qName,
                             const QXmlAttributes &att){


        if (!giTag && qName != "gi"){
                erreur = QObject::tr("This file is not supported, gi required");
                return false;
        }

        if (qName == "gi"){
                QString v = att.value("version");
                if (!v.isEmpty() && v != "1.0"){
                        erreur = QObject::tr("This file is not a GI version 1.0 file");
                        return false;
                }
                giTag = true;
        }

        else if(qName =="graph"){
                QString v = att.value("id");
                if (v.isEmpty())
                        v = "untitled";

                graph->setName(v);
        }

        else if(qName =="node"){
                current_node = att.value("id");
		c_node = new Node(graph,current_node);
                int x = att.value("x").toInt();
                int y = att.value("y").toInt();
		graph->add(c_node,QPoint(x,y));
        }

        else if(qName =="edge"){
                QString n1 = att.value("source");
                QString n2 = att.value("dest");
                QString type = att.value("type");
                int dir = att.value("direction").toInt();
                graph->link(n1,n2);
        }

	/**
        else if (NodeContent::typeByName( qName ) > 0) {
		int t = NodeContent::typeByName( qName );
		NodeContent *c = new NodeContent(c_node,t,nbr++);	
		if (c->isNull()){
			erreur = QObject::tr("Cannot load " + qName + "content");
			return false;
		}
		QString url = att.value("url");
		if (!url.isEmpty()){
			c->open( url );
			c_node->push(c);
		}
        }*/

        buffer = "";
        return true;
}

bool GIHandler::endElement  (const QString &,
                             const QString &,
                             const QString &qName){
        if (qName == "gi")
                graph->repaint();
        else if(qName =="node"){
                current_node = "";
		c_node = 0;
        }
        return true;
}

bool GIHandler::characters  (const QString &txt){
        buffer += txt;
        return true;
}

bool GIHandler::fatalError (const QXmlParseException &exception){
        QMessageBox::critical (graph,
                                QObject::tr("Graph Interface Parser"),
                                QObject::tr("Parse error at line %1, column %2:\n""%3")
                                .arg(exception.lineNumber())
                                .arg(exception.columnNumber())
                                .arg(exception.message()));
        return false;
}

QString GIHandler::errorString() const{
        return erreur;
}



