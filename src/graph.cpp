/***************************************************************************
 *   Copyright (C) 2007 by Schpilka Yannick   *
 *   schpilka@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 *   In addition, as a special exception, the copyright holders give       *
 *   permission to link the code of this program with any edition of       *
 *   the Qt library by Trolltech AS, Norway (or with modified versions     *
 *   of Qt that use the same license as Qt), and distribute linked         *
 *   combinations including the two.  You must obey the GNU General        *
 *   Public License in all respects for all of the code used other than    *
 *   Qt.  If you modify this file, you may extend this exception to        *
 *   your version of the file, but you are not obligated to do so.  If     *
 *   you do not wish to do so, delete this exception statement from        *
 *   your version.                                                         *
 ***************************************************************************/
#include "graph.h"
#include "edge.h"
#include <qpopupmenu.h>
#include <qcolordialog.h>
#include <qinputdialog.h>
#include <qdragobject.h>
#include <qcursor.h>
#include "node.h"
#include <qtooltip.h>
#include <qpainter.h>
#include <qpen.h>
#include <qpoint.h>

Graph::Graph(QWidget *parent,QString id):QScrollView(parent,id){
        //On rend la zone accessible
        setAcceptDrops(true);

        //Taille du contenu
        resizeContents(5000,5000);

        //Mode normal de visualisation
        drawing = false;

        //Les mise a jour lors des signaux
        connect(this,SIGNAL(nodeAdded()),
                this,SLOT(adjustTooltip()));
        //Les mise a jour lors des signaux
        connect(this,SIGNAL(edgeCreated()),
                this,SLOT(adjustTooltip()));

        //Signaux necessitant un refresh
        connect(this,SIGNAL(nodeAdded()),
                this,SLOT(update()));
        //Quand le scroll bouge on redresh pour les liens
        connect(this,SIGNAL(contentsMoving (int,int)),
                this,SLOT(update()));

        connect(this,SIGNAL(edgeCreated()),
                this,SLOT(update()));


        adjustTooltip();
        initContextMenu();
}


Graph::~Graph(){
}

/**Ajouter un noeud dans le graphe*/
void Graph::operator<<( Node *node ){
        reparent(viewport(),QPoint(0,0),true);
        addChild(node);
	nodes << QString(node->name());
	node->setStackColor( viewport()->paletteBackgroundColor());
        node->move(300,300);
        node->show();
        emit nodeAdded();
}

void Graph::add (Node *node,QPoint p){
	addChild(node);
        nodes << QString(node->name());
	node->setStackColor( viewport()->paletteBackgroundColor());
        node->move(p);
        node->show();
	connect(node,SIGNAL(nodeMoved()),
                this,SLOT(update()));

        emit nodeAdded();
}

/**Lie deux noeuds*/
void Graph::link(Node *source, Node *cible ){
        edges.append(new Edge(source, cible));
        emit edgeCreated();
}

/**Lie deux noeuds*/
void Graph::link(QString source, QString cible ){
        edges.append(new Edge((Node*)child(source), (Node*)child(cible)));
        emit edgeCreated();
}

/** Creation d'un noeud a l'aide de boite de dialogue*/
void Graph::createNode(){
        bool ok;
        QString text = QInputDialog::getText(
            "KGraphInterface Node Edition", "Enter node name:", QLineEdit::Normal,
             QString::null, &ok, this );
        if (text.isEmpty() || !ok || nodes.contains(text))
                return;

        Node *node = new Node(this,text);
        connect(node,SIGNAL(nodeMoved()),
                this,SLOT(update()));

        //Creation d'un lien
        connect(node,SIGNAL(createEdge(QString)),
                this,SLOT(beginEdge(QString)));

        //Creation d'un lien
        connect(node,SIGNAL(endEdge(QString)),
                this,SLOT(endEdge(QString)));

        addChild(node);
        nodes << QString(node->name());
	node->setStackColor( viewport()->paletteBackgroundColor());
        node->move(300,300);
        node->show();
        emit nodeAdded();
}

/** Creation d'un lien a l'aide de boite de dialogue*/
void Graph::createEdge(){
        bool ok;

        // id source
        QString src = QInputDialog::getText(
            "KGraphInterface Edge Edition", "Enter node source id:", QLineEdit::
Normal,
             QString::null, &ok, this );

        if (src.isEmpty() || !ok)
                return;

        // id cible
        QString dest = QInputDialog::getText(
            "KGraphInterface Edge Edition", "Enter node target id:", QLineEdit::
Normal,
             QString::null, &ok, this );

        if (dest.isEmpty() || !ok)
                return;

        // type
        QString type = QInputDialog::getText(
            "KGraphInterface Edge Edition", "Enter edge type:", QLineEdit::Normal,
             QString::null, &ok, this );

        if (!ok)
                return;

        if (type.isEmpty())
                type = "undirected";

        if (nodes.contains(src) && nodes.contains(dest))
                link((Node*)child(src),(Node*)child(dest));
}

/** Commencer la creation d'un lien a partir d'un noeud source*/
void Graph::beginEdge(QString source){
        if (child(source) == 0)
                return;
        //mode dessin
        drawing = true;
        //centre du noeud surce
        start_drawing_edge = ((QWidget*)child(source))->pos();
        //on retient l'id
        source_drawing_edge = source;
        //on observe les mouvements de la souris
        viewport()->setMouseTracking(true);
}

/** Terminer la creation d'un lien a partir d'un noeud cible*/
void Graph::endEdge(QString target){
        if (!drawing)
                return;

        if (child(target) == 0){
                cancelEdge();
                return;
        }
	if (target == source_drawing_edge)
		return;

        link((Node*)child(source_drawing_edge),(Node*)child(target));
        cancelEdge();
}

/** Annuler la creation d'un lien*/
void Graph::cancelEdge(){
        QPoint p(0,0);
        start_drawing_edge = p;
        viewport()->setMouseTracking(false);
        drawing = false;
        update();
}

/** Montre une vue d'ensemble du graphe */
void Graph::showOverview(){

}

/** Ouvrir une boite de dialogue de choix de couleur*/
void Graph::openColorDlg(){
        QColor c;
        c = QColorDialog::getColor(c,this,"colordlg");
}

/**Dessin du graphe*/
void Graph::paintEvent(QPaintEvent *ev){
        viewport()->repaint();

        QPainter painter(viewport());
        painter.setWindow(viewport()->rect());
	
	QColor color = viewport()->paletteBackgroundColor();
        //on inverse la couleur de fond pour ecrire le titre
        int r = 255 - color.red();
        int g = 255 - color.green();
        int b = 255 - color.blue();

        painter.setPen(QColor(r,g,b));


        painter.save();
        //Nom du graphe toujours visible
        painter.drawText(QPoint(30,30),"Graph : " + QString(name()));

        if (drawing)
                painter.drawLine(start_drawing_edge,viewport()->mapFromGlobal(QCursor::pos()));

        //Edges
        Edge *e;
        for ( e = edges.first(); e; e = edges.next() )
                e->draw(&painter);

        painter.restore();
}

/** Drag d'un noeud ou couleur*/
void Graph::dragEnterEvent(QDragEnterEvent *event){
        if (QColorDrag::canDecode(event))
                event->accept();
}

/** Drop d'un noeud ou couleur */
void Graph::dropEvent(QDropEvent *event){
        QColor c;
        if ( QColorDrag::decode(event, c) ){
                viewport()->setPaletteBackgroundColor(c);
                for ( QStringList::Iterator it = nodes.begin(); it != nodes.end(
); ++it )
                        ((Node*)child(*it))->setStackColor(c);
        }
	update();
}

/** Met a jour le tooltip */
void Graph::adjustTooltip(){
        QString strInfos;
        strInfos = "Graph\n";
        strInfos.append("\nName:        " + QString(name()));
        strInfos.append("\nNbr nodes:   " + QString::number(nodes.size()));
        strInfos.append("\nNbr edges:   " + QString::number(edges.count()));
        QToolTip::add( this, strInfos);
}

/** Initialisation du menu contextuel */
void Graph::initContextMenu(){
        context_menu = new QPopupMenu;
        context_menu->insertItem( "create node",  this, SLOT(createNode()));
        context_menu->insertItem( "create edge", this, SLOT(createEdge()));
        context_menu->insertItem( "open color dialog", this, SLOT(openColorDlg()                              ));
}

/** On bouge la souris dans le graphe (perçu si mousetracking true)*/
void Graph::contentsMouseMoveEvent ( QMouseEvent * e ){
        update();
}

/** Pop du menu contextuel*/
void Graph::mouseReleaseEvent(QMouseEvent *event){
	if (drawing){
        	cancelEdge();
		return;
	}
        if (event->button() == Qt::RightButton){
                context_menu->exec( QCursor::pos());
        }
}

QString Graph::toXml(){
	QString res;

        /**Graphe*/
        res = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        res.append("\n<!DOCTYPE gi>");
        res.append("\n<gi version=\"1.0\">");
        res.append("\n     <graph id=\"" + QString(name()) + "\">");

	res.append("<!-- Nodes Definition -->");

          for ( QStringList::Iterator it = nodes.begin(); it != nodes.end(
); ++it )
        	res.append(((Node*)child(*it))->toXml());

        /**Liens*/

	res.append("<!-- Nodes Definition -->");

	Edge *e;
        for ( e = edges.first(); e; e = edges.next() ){
                res.append(e->toXml());
        }

        res.append("\n     </graph>");
        res.append("\n</gi>\n");
        return res;

}

#include "graph.moc"
