<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="class">
    <name>KGraphInterface</name>
    <filename>classKGraphInterface.html</filename>
    <member kind="function">
      <type></type>
      <name>KGraphInterface</name>
      <anchorfile>classKGraphInterface.html</anchorfile>
      <anchor>c71f9cbdca06084e6924cf4426f90eff</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~KGraphInterface</name>
      <anchorfile>classKGraphInterface.html</anchorfile>
      <anchor>dfb364f37d13c1ef372df90818d4e2c0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>load</name>
      <anchorfile>classKGraphInterface.html</anchorfile>
      <anchor>a3b49ca6ef5418039c0e4674fa320d8e</anchor>
      <arglist>(const KURL &amp;url)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>saveProperties</name>
      <anchorfile>classKGraphInterface.html</anchorfile>
      <anchor>76182cd4faa12c4142979e6c773483de</anchor>
      <arglist>(KConfig *)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>readProperties</name>
      <anchorfile>classKGraphInterface.html</anchorfile>
      <anchor>65cabf77a7b059f9811b5ab291db475a</anchor>
      <arglist>(KConfig *)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>KGraphInterfacePart</name>
    <filename>classKGraphInterfacePart.html</filename>
    <member kind="function">
      <type></type>
      <name>KGraphInterfacePart</name>
      <anchorfile>classKGraphInterfacePart.html</anchorfile>
      <anchor>81c922713f610f9f3429ed6c9331ebe3</anchor>
      <arglist>(QWidget *parentWidget, const char *widgetName, QObject *parent, const char *name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~KGraphInterfacePart</name>
      <anchorfile>classKGraphInterfacePart.html</anchorfile>
      <anchor>28d9cc6a41fac98c04e4adeef6e8cd07</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setReadWrite</name>
      <anchorfile>classKGraphInterfacePart.html</anchorfile>
      <anchor>dc41fc8650d1986825d398083490d183</anchor>
      <arglist>(bool rw)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setModified</name>
      <anchorfile>classKGraphInterfacePart.html</anchorfile>
      <anchor>8dd80fc1bd380be409dae0eec6e31378</anchor>
      <arglist>(bool modified)</arglist>
    </member>
    <member kind="slot" protection="protected">
      <type>void</type>
      <name>fileOpen</name>
      <anchorfile>classKGraphInterfacePart.html</anchorfile>
      <anchor>d15d03254f3b3471f3cf7c198dbf56de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot" protection="protected">
      <type>void</type>
      <name>fileSaveAs</name>
      <anchorfile>classKGraphInterfacePart.html</anchorfile>
      <anchor>3d4d3d19781c8ab1a6899d3f6fd8814c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual bool</type>
      <name>openFile</name>
      <anchorfile>classKGraphInterfacePart.html</anchorfile>
      <anchor>ec34a8c50e649bf19d82ae4f64b79ae4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual bool</type>
      <name>saveFile</name>
      <anchorfile>classKGraphInterfacePart.html</anchorfile>
      <anchor>4c108d73733e8ad1e3efb43b4a83abed</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>NodeContent</name>
    <filename>classNodeContent.html</filename>
    <member kind="enumvalue">
      <type>@</type>
      <name>Text</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>ba2320c6105f963fca3a9ffedfec7f5541e03c8fe8ba7ead6f2b2a1a231ce964</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>Html</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>ba2320c6105f963fca3a9ffedfec7f5550de76a19e4c42108ffa9c3508dfdf2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>windowClosed</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>2ede661da81f46f5ee7354bf20f4e2a9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>unpopWidget</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>21e23187118ba246cc2e238809a1e428</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>popWidget</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>b63c39762f6eef136863ab1579f14586</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>partNotLoad</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>c6f493de4c33d2225bfc6849dd447eb3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NodeContent</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>1e213fbd97890892ec6eb4c5ff152bd8</anchor>
      <arglist>(Node *node, QString name, int type)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>save</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>0ba4d6ec4eb72f60cf71ef799f7bfe24</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>toXml</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>5308c1574e52a308652528bbec86f016</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>QWidget *</type>
      <name>getWidget</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>31c1ea399b541e0cf0b804c76c362b23</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getName</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>e55eab0a797aa5382d8efa21020c1993</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getType</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>c836f6f71e25f099831a6cafcb189ce9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>QWidget *</type>
      <name>widget</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>8e0f960d19b9283fdd9ea12a3aa67e86</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Node *</type>
      <name>parent_node</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>598969cf4e0a4a8518e19224f469bc3e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>ContentWindow *</type>
      <name>window</name>
      <anchorfile>classNodeContent.html</anchorfile>
      <anchor>14b1b45d5c252ae13c802a466b66eb0b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>kgraphinterface/</name>
    <path>/home/schpilka/kgraphinterface/</path>
    <filename>dir_13bf8767770a07be1dadcd74264f09a2.html</filename>
    <dir>kgraphinterface/src/</dir>
    <file>config.h</file>
  </compound>
  <compound kind="dir">
    <name>kgraphinterface/src/</name>
    <path>/home/schpilka/kgraphinterface/src/</path>
    <filename>dir_cd0dc5b52ac10de8cdc9686ef8f66f03.html</filename>
    <file>contentwindow.cpp</file>
    <file>contentwindow.h</file>
    <file>contentwindow.moc</file>
    <file>edge.cpp</file>
    <file>edge.h</file>
    <file>graph.cpp</file>
    <file>graph.h</file>
    <file>graph.moc</file>
    <file>kgraphinterface.cpp</file>
    <file>kgraphinterface.h</file>
    <file>kgraphinterface.moc</file>
    <file>kgraphinterface_part.cpp</file>
    <file>kgraphinterface_part.h</file>
    <file>kgraphinterface_part.moc</file>
    <file>main.cpp</file>
    <file>node.cpp</file>
    <file>node.h</file>
    <file>node.moc</file>
    <file>nodecontent.cpp</file>
    <file>nodecontent.h</file>
    <file>nodecontent.moc</file>
  </compound>
</tagfile>
